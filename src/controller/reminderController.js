const { STATUS_CODE } = require("../constants/item.constants");

const db = require("../db/models/index");
/*---------------------------------*/
const { Reminder, Tree, Plants, TreeDiary } = db;

// ----------- AREAS ---------------------
const getOwnReminder = async (userId) => {
  try {
    let reminders = await Reminder.findAll({
      where: {
        user_id: userId,
      },
      include: {
        model: Tree,
        as: "trees",
        include: {
          model: Plants,
          as: "plant",
          attributes: ["avatar"],
        },
        attributes: ["id", "name"],
      },
    });
    let data = reminders.map((d) => d.dataValues);

    for (let i = 0; i < data.length; i++) {
      const trees = data[i].trees.map((t) => t.dataValues);
      for (let j = 0; j < trees.length; j++) {
        const lastDiaries = await TreeDiary.findOne({
          where: { tree_id: trees[j].id },
          order: [["date_published", "DESC"]],
        });
        trees[j].status = lastDiaries ? lastDiaries.dataValues.status : 0;
      }
    }
    return data;
  } catch (error) {}
};

exports.getReminder = async (req, res) => {
  try {
    const userId = req.userId;
    const data = await getOwnReminder(userId);
    res.json(data);
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

exports.createReminder = async (req, res) => {
  try {
    const user_id = req.userId;
    let { date_start, date_end, time, loop, note, trees } = req.body;
    const newTime = new Date(time);

    date_start = new Date(date_start);
    date_start.setTime(date_start.getTime() + 7 * 60 * 60 * 1000);

    date_end = new Date(date_end);
    date_end.setTime(date_end.getTime() + 7 * 60 * 60 * 1000);

    console.log(date_start, date_end, time);

    const newReminder = {
      user_id,
      date_start,
      date_end,
      time: `${newTime.getHours() < 10 ? "0" : ""}${newTime.getHours()}:${
        newTime.getMinutes() < 10 ? "0" : ""
      }${newTime.getMinutes()}`,
      loop,
      note,
    };

    const created = await Reminder.upsert(newReminder);

    let r;

    if (created && created[0]) {
      const reminder = created[0];
      r = await Promise.all(trees.map((tree) => reminder.addTree(tree)));
    }
    if (r) {
      const data = await getOwnReminder(user_id);
      res.status(STATUS_CODE.OK).json(data);
    } else {
      res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
    }
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

exports.deleteReminder = async (req, res) => {
  try {
    const { id } = req.params;
    const deleted = await Reminder.destroy({ where: { id } });
    if (deleted) {
      const userId = req.userId;
      const data = await getOwnReminder(userId);
      res.status(STATUS_CODE.OK).json(data);
    } else {
      res.status(STATUS_CODE.NOT_FOUND).json("Not Found");
    }
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

exports.updateReminder = async (req, res) => {
  const { id } = req.params;
  try {
    const user_id = req.userId;
    let { date_start, date_end, time, loop, note, trees } = req.body;
    const newTime = new Date(time);
    time = `${newTime.getHours() < 10 ? "0" : ""}${newTime.getHours()}:${
      newTime.getMinutes() < 10 ? "0" : ""
    }${newTime.getMinutes()}`;

    let newReminder = await Reminder.findByPk(id);
    newReminder.date_start = date_start;
    newReminder.date_end = date_end;
    newReminder.time = time;
    newReminder.loop = loop;
    newReminder.note = note;
    newReminder = await newReminder.save();

    await newReminder.setTrees([]);
    const newTrees = await Tree.findAll({ where: { id: trees } });
    await newReminder.addTrees(newTrees);
    const updatedReminder = await Reminder.findOne({
      where: { id },
      include: {
        model: Tree,
        as: "trees",
        include: {
          model: Plants,
          as: "plant",
          attributes: ["avatar"],
        },
        attributes: ["id", "name"],
      },
    });

    res.json(updatedReminder);
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};
