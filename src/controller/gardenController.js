const { STATUS_CODE } = require("../constants/item.constants");
const { Sequelize, Op, where } = require("sequelize");
const db = require("../db/models/index");
const moment = require("moment");
/*---------------------------------*/
const { Area, Tree, Reminder, TreeDiary, PlantStage, Plants } = db;

// ----------- AREAS ---------------------

const getArray = (dataArray) => {
  const newArray = [];
  dataArray.forEach((item) => {
    newArray.push(item.dataValues);
  });
  return newArray;
};

exports.getAreas = async (req, res) => {
  try {
    let areas = getArray(
      await Area.findAll({
        where: {
          user_id: req.userId,
        },
        order: [["no"]],
      })
    );
    res.json(areas);
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

exports.createArea = async (req, res) => {
  try {
    let { name } = req.body;
    name = name.trim();
    const isExist = await Area.findOne({
      where: { name, user_id: req.userId },
    });
    if (isExist) {
      res.status(STATUS_CODE.BAD_REQUEST).json("Tên đã tồn tại");
    } else {
      const noMax = await Area.max("no", { where: { user_id: req.userId } });
      const newArea = await Area.upsert({
        name,
        user_id: req.userId,
        no: noMax ? noMax + 1 : 1,
      });
      res.json(newArea[0]);
    }
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

exports.updateArea = async (req, res) => {
  try {
    const { id } = req.params;
    let { name } = req.body;
    name = name.trim();
    const isExist = await Area.findOne({
      where: { name, user_id: req.userId, id: { [Op.ne]: id } },
    });
    if (isExist) {
      res.status(STATUS_CODE.BAD_REQUEST).json("Tên đã tồn tại");
    } else {
      await Area.update(
        { name },
        {
          where: { id },
        }
      );
      const area = await Area.findByPk(id);
      res.json(area);
    }
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

exports.deleteArea = async (req, res) => {
  try {
    const { id } = req.params;
    const changeNullArea = await Tree.update(
      { area_id: null },
      {
        where: {
          area_id: id,
        },
      }
    );
    const deleted = await Area.destroy({ where: { id } });

    const garden = await getUserGarden(req.userId);
    res.json(garden);
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

const getUserGarden = async (userId) => {
  let areas = getArray(
    await Area.findAll({
      where: {
        user_id: userId,
      },
      order: [["no"]],
    })
  );

  areas.push({ id: null, name: "KHÁC" });
  const newAreas = [];

  for (let i = 0; i < areas.length; i++) {
    let trees = getArray(
      await Tree.findAll({
        where: {
          user_id: userId,
          area_id: areas[i].id,
        },
        order: [["id"]],
        include: "plant",
      })
    );

    for (let i = 0; i < trees.length; i++) {
      const lastDiaries = await TreeDiary.findOne({
        where: { tree_id: trees[i].id },
        order: [["date_published", "DESC"]],
      });
      trees[i].status = lastDiaries ? lastDiaries.dataValues.status : -1;
    }

    newAreas.push({ ...areas[i], trees });
  }
  return newAreas;
};

exports.getGarden = async (req, res) => {
  try {
    const garden = await getUserGarden(req.userId);
    res.json(garden);
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

exports.getTreeStages = async (req, res) => {
  try {
    const { tree_id } = req.params;
    const treeEntity = await Tree.findOne({ where: { id: tree_id } });
    const latestDiary = await TreeDiary.findAll({
      where: { tree_id: tree_id },
      order: [["stage_no", "DESC"]],
      limit: 1,
    });
    if (latestDiary.length > 0) {
      const stages = await PlantStage.findAll({
        where: {
          plant_id: latestDiary[0].plant_id,
        },
        order: ["no"],
      });
      res.json(stages);
    } else {
      const stages = await PlantStage.findAll({
        where: { plant_id: treeEntity.plant_id },
        order: ["no"],
      });
      res.json(stages);
    }
  } catch (error) {
    res.status(STATUS_CODE.INTERNAL_ERROR).json({ message: error });
  }
};

exports.addTree = async (req, res) => {
  try {
    const {
      name,
      plant: plant_id,
      area: area_id,
      description,
      date_created,
    } = req.body;
    const isExist = await Tree.findOne({
      where: { user_id: req.userId, name: name },
    });
    if (isExist) {
      res.status(STATUS_CODE.BAD_REQUEST).json("Tên đã tồn tại");
    } else {
      const add = await Tree.upsert({
        name,
        user_id: req.userId,
        plant_id,
        area_id,
        description,
        status: 1,
      });
      const newId = add[0].id;
      if (date_created) {
        const firstDiary = {
          tree_id: newId,
          stage_no: 1,
          plant_id,
          note: "Cây được trồng",
          status: 0,
          date_published: date_created,
          weather: "Nắng",
        };
        const saveRes = await TreeDiary.upsert(firstDiary);
      }
      const newTree = await Tree.findOne({
        where: {
          id: newId,
        },
        include: "plant",
      });
      res.json(newTree);
    }
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

exports.deleteTree = async (req, res) => {
  try {
    const { id } = req.params;
    const deleted = await Tree.destroy({ where: { id } });
    res.json("Đã xóa");
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

function insertDiaryToData(diary, stage) {
  let _di = null;
  let _ti = "";
  let _stg = null;
  if (diary !== null) {
    _di = {
      id: diary.id,
      title: diary.status,
      sub_title: diary.weather,
      img: diary.image_url,
      note: diary.note,
      stage_no: diary.stage_no,
    };
    _ti = diary.date_published;
  }
  if (stage !== null) {
    _stg = {
      id: stage.id,
      title: stage.name,
      sub_title: `${stage.range} ngày`,
      note: stage.description,
      date_estimate: stage.date_estimate,
    };
  }
  return {
    diary: _di,
    time: _ti,
    stage: _stg,
  };
}

exports.getTree = async (tree_id) => {
  const treeEntity = await Tree.findOne({
    attributes: ["id", "name", "plant_id", "area_id", "description"],
    where: { id: tree_id },
    include: [
      {
        model: Plants,
        as: "plant",
      },
      {
        model: Area,
        as: "area",
      },
      {
        model: Reminder,
        as: "reminders",
      },
    ],
  });
  if (!treeEntity) {
    return { tree: null, diaries: [] };
  } else {
    const plant_id = treeEntity.plant_id;

    const found_stages = await PlantStage.findAll({
      where: { plant_id: plant_id },
      order: ["no"],
    });
    if (!found_stages) {
      const diaries = await TreeDiary.findAll({ order: ["date_published"] });

      // NO PLANT STAGE AND NO DIARY FOUND
      if (diaries === 0) return { tree: treeEntity, diaries: [] };
      // HAVE DIARY WITH NO STAGE
      else {
        const data = [];
        for (let diary of diaries) {
          data.push(insertDiaryToData(diary, null));
        }
        return { tree: treeEntity, diaries: data };
      }
    } else {
      let data = [];
      const firstDiary = await TreeDiary.findOne({
        where: { plant_id, tree_id },
        order: ["date_published"],
      });
      if (!firstDiary) {
        for (let stage of found_stages) {
          stage["date_estimate"] = "";
          data.push(insertDiaryToData(null, stage));
        }
        return { tree: treeEntity, diaries: data };
      } else {
        data = await get_diary_estimate_time(
          firstDiary,
          found_stages,
          plant_id,
          tree_id
        );
        // END OF FUNCTION
        return { tree: treeEntity, diaries: data };
      }
    }
  }
};

async function get_first_date_of_stage(plant_id, tree_id, stage_no) {
  const firstDiary = await TreeDiary.findOne({
    where: { plant_id, tree_id, stage_no },
    order: ["date_published"],
  });
  if (!firstDiary) {
    return null;
  } else {
    return new Date(Date.parse(firstDiary?.dataValues["date_published"]));
  }
}

async function get_diary_estimate_time(firstDiary, stages, plant_id, tree_id) {
  let data = [];

  let flag_date = new Date(
    Date.parse(firstDiary["dataValues"]["date_published"])
  );
  const startStageNo = parseInt(firstDiary["dataValues"]["stage_no"]);

  for (let stage of stages) {
    let is_first = true;

    const expert_date =
      stage["no"] >= startStageNo ? moment(flag_date).format("DD/MM/YYYY") : "";

    const receive_date = await get_first_date_of_stage(
      plant_id,
      tree_id,
      stage["no"]
    );

    if (receive_date === null) {
      stage["date_estimate"] = expert_date;
      stage["msg_estimate"] = "";
    } else {
      const diff = (receive_date - flag_date) / 1000 / 60 / 60 / 24;
      stage["date_estimate"] =
        diff > 0
          ? `${moment(receive_date).format(
              "DD/MM/YYYY"
            )}, <span class="time-estimate-less-than">Trễ hơn ${diff} ngày dự kiến.</span>`
          : diff < 0
          ? `${moment(receive_date).format(
              "DD/MM/YYYY"
            )}, <span class="time-estimate-greater-than">Sớm hơn ${-diff} ngày dự kiến</span>`
          : moment(receive_date).format("DD/MM/YYYY");
      flag_date = receive_date;
    }

    const diaries = await TreeDiary.findAll({
      where: { stage_no: stage.no, plant_id: plant_id, tree_id },
      order: ["date_published"],
    });
    if (diaries.length === 0) {
      data.push(insertDiaryToData(null, stage));
    } else {
      for (let diary of diaries) {
        if (is_first) {
          is_first = false;
          data.push(insertDiaryToData(diary, stage));
        } else {
          data.push(insertDiaryToData(diary, null));
        }
      }
    }
    // estimate time in next stage
    if (stage["date_estimate"] !== "")
      flag_date.setDate(flag_date.getDate() + stage.range);
  }
  return data;
}

async function getTree(tree_id) {
  const treeEntity = await Tree.findOne({
    attributes: ["id", "name", "plant_id", "area_id", "description"],
    where: { id: tree_id },
    include: [
      {
        model: Plants,
        as: "plant",
      },
      {
        model: Area,
        as: "area",
      },
      {
        model: Reminder,
        as: "reminders",
      },
    ],
  });
  if (!treeEntity) {
    return { tree: null, diaries: [] };
  } else {
    const plant_id = treeEntity.plant_id;

    const found_stages = await PlantStage.findAll({
      where: { plant_id: plant_id },
      order: ["no"],
    });
    if (!found_stages) {
      const diaries = await TreeDiary.findAll({ order: ["date_published"] });

      // NO PLANT STAGE AND NO DIARY FOUND
      if (diaries === 0) return { tree: treeEntity, diaries: [] };
      // HAVE DIARY WITH NO STAGE
      else {
        const data = [];
        for (let diary of diaries) {
          data.push(insertDiaryToData(diary, null));
        }
        return { tree: treeEntity, diaries: data };
      }
    } else {
      const data = [];
      const firstDiary = await TreeDiary.findOne({
        where: { plant_id, tree_id },
        order: ["date_published"],
      });
      if (!firstDiary) {
        for (let stage of found_stages) {
          stage["date_estimate"] = "";
          data.push(insertDiaryToData(null, stage));
        }
        return { tree: treeEntity, diaries: data };
      } else {
        const firstDate = new Date(
          Date.parse(firstDiary["dataValues"]["date_published"])
        );
        const startStageNo = parseInt(firstDiary["dataValues"]["stage_no"]);
        for (let stage of found_stages) {
          let is_first = true;
          if (stage["no"] >= startStageNo) {
            stage["date_estimate"] = moment(firstDate).format("YYYY-MM-DD");
          } else {
            stage["date_estimate"] = "";
          }

          const diaries = await TreeDiary.findAll({
            where: { stage_no: stage.no, plant_id: plant_id, tree_id },
            order: ["date_published"],
          });
          if (diaries.length === 0) {
            data.push(insertDiaryToData(null, stage));
          } else {
            for (let diary of diaries) {
              if (is_first) {
                is_first = false;
                data.push(insertDiaryToData(diary, stage));
              } else {
                data.push(insertDiaryToData(diary, null));
              }
            }
          }
          if (stage["date_estimate"] !== "")
            firstDate.setDate(firstDate.getDate() + stage.range);
        }
        // END OF FUNCTION
        return { tree: treeEntity, diaries: data };
      }
    }
  }
}
// exports.getDiary = async (req, res) => {
//   try {
//     const { tree_id } = req.params;
//     const treeInfo = await getTree(tree_id);
//     return res.json(treeInfo);
//   } catch (error) {
//     console.error(error);
//     res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
//   }
// };

exports.updateDiary = async (req, res) => {
  try {
    const { diary_id } = req.params;
    const image_url = req.body?._img || req.file?.path || null;
    const { note, weather, status } = req.body;
    const newDiary = {
      note,
      weather,
      status,
      image_url,
    };
    const saveRes = await TreeDiary.update(newDiary, {
      where: {
        id: diary_id,
      },
    });
    if (saveRes.length > 0) {
      const _diary = await TreeDiary.findOne({ where: { id: diary_id } });
      const newData = await getTree(_diary.tree_id);
      res.json(newData);
    } else {
      res.status(404).json("Diary not found!");
    }
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};

exports.updateTree = async (req, res) => {
  try {
    const { id } = req.params;
    const { name, note: description } = req.body;
    if (name) {
      const isExist = await Tree.findOne({
        where: { user_id: req.userId, name: name, id: { [Op.ne]: id } },
      });
      if (isExist) {
        res.status(STATUS_CODE.BAD_REQUEST).json("Tên đã tồn tại");
      }
    }
    const body = req.body;
    await Tree.update(body, {
      where: { id },
    });
    const tree = await Tree.findByPk(id);
    res.json(tree);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
};
