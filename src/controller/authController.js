const jwt = require("jsonwebtoken");
const { promisify } = require("util");
const userController = require("../controller/userController");
const { ErrorMessage } = require("../models/commonResponse");
const { MESSAGE_CONST } = require("../constants/message.constants");
const { STATUS_CODE } = require("../constants/item.constants");
const moment = require("moment");
/* end of import */

/*
 * create new token
 * @param payload : object
 */
const signToken = async (payload) => {
  const token = await jwt.sign(
    { id: payload.id, email: payload.email, password: payload.password },
    process.env.JWT_SECRET,
    {
      expiresIn: "30d",
    }
  );
  return token;
};

/*
 * create token and send to client
 * @param payload : object
 */
const createSendToken = async (payload, res) => {
  try {
  } catch (error) {
    res.status(404).json({ message: "Lỗi khi truyền param token" });
  }
};

/*
 * SIGN UP
 */
exports.signup = async (req, res, next) => {
  const { email, password, full_name } = req.body;
  if (!email || !password || !full_name) {
    return res
      .status(STATUS_CODE.BAD_REQUEST)
      .send(
        new ErrorMessage(MESSAGE_CONST.REQUIRED_VALUE.code, [
          "Email/ PassWord/ Full name",
        ])
      );
  }
  const isExits = await userController.isExits({ email });
  if (isExits > 0) {
    return res
      .status(400)
      .send(new ErrorMessage(MESSAGE_CONST.IS_EXISTED.code, ["Email", email]));
  } else {
    try {
      const date = new Date();
      const newUser = await userController.create({
        email,
        password,
        full_name,
        create_at: date.toISOString(),
      });
      return res.status(200).send("success");
    } catch (error) {
      console.log(error.message);
      return res.status(500).send({ message: "Server error!" });
    }
  }
};

/*
 * LOG IN
 *
 */
exports.login = async (req, res, next) => {
  const { email, password } = req.body;
  // 1) Check if email and password exist
  if (!email || !password) {
    return res
      .status(STATUS_CODE.BAD_REQUEST)
      .send(
        new ErrorMessage(MESSAGE_CONST.REQUIRED_VALUE.code, ["Email/ PassWord"])
      );
  }
  try {
    const user = await userController.getOne({ email, password });
    if (!user) {
      return res
        .status(STATUS_CODE.NOT_FOUND)
        .json("Tài khoản hoặc mật khẩu chưa đúng!");
    }
    const token = await signToken(user);
    res.status(200).json({ token, user });
  } catch (error) {
    console.log(error);
    return res.status(500).send({ message: "Server error" });
  }
};

/*
 * Protect gateway to other api
 *
 */
exports.protect = async (req, res, next) => {
  // 1) Getting token and check of it's there
  let token = "";
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith("Bearer")
  ) {
    token = req.headers.authorization.split(" ")[1];
  }
  if (!token) {
    return res
      .status(STATUS_CODE.TOKEN_ERR)
      .send(new ErrorMessage(MESSAGE_CONST.NOT_FOUND.code, ["token"]));
  } else {
    try {
      // 2) Verification token
      const decoded = await jwt.verify(token, process.env.JWT_SECRET);
      const userToken = decoded;

      // console.log("decode : ", decoded);
      if (!userToken) {
        return res
          .status(STATUS_CODE.TOKEN_ERR)
          .send(
            new ErrorMessage(MESSAGE_CONST.NOT_FOUND.code, ["payload token"])
          );
      } else {
        // 3) Check if user still exists
        const currentUser = await userController.getOne({
          id: userToken.id,
          email: userToken.email,
          password: userToken.password,
        });

        if (!currentUser) {
          // not found user in db
          return res
            .status(STATUS_CODE.TOKEN_ERR)
            .send(
              new ErrorMessage(MESSAGE_CONST.NOT_PERMISSION.code, [
                "truy cập api",
              ])
            );
        } else {
          req.userId = currentUser.id;
          return next();
        }
      }
    } catch (error) {
      return res
        .status(STATUS_CODE.TOKEN_ERR)
        .send(new ErrorMessage(MESSAGE_CONST.IN_VALID.code, ["token"]));
    }
  }
};

/*
 * check token
 *
 */
exports.initPermission = async (req, res, next) => {
  // 1) Getting token and check of it's there
  let token;
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith("Bearer")
  ) {
    token = req.headers.authorization.split(" ")[1];
  }
  if (!token) {
    return res
      .status(STATUS_CODE.TOKEN_ERR)
      .send(new ErrorMessage(MESSAGE_CONST.NOT_FOUND.code, ["token"]));
  } else {
    try {
      // 2) Verification token
      const decoded = await jwt.verify(token, process.env.JWT_SECRET);
      const userToken = decoded;
      // console.log("decode : ", decoded);
      if (!userToken) {
        return res
          .status(STATUS_CODE.TOKEN_ERR)
          .send(
            new ErrorMessage(MESSAGE_CONST.NOT_FOUND.code, ["payload token"])
          );
      } else {
        // 3) Check if user still exists
        const currentUser = await userController.getOne({
          id: userToken.id,
          email: userToken.email,
          password: userToken.password,
        });

        if (!currentUser) {
          // not found user in db
          return res
            .status(STATUS_CODE.TOKEN_ERR)
            .send(
              new ErrorMessage(MESSAGE_CONST.NOT_PERMISSION.code, [
                "truy cập api",
              ])
            );
        } else {
          return res.status(200).send({ ...currentUser });
        }
      }
    } catch (error) {
      console.log("err decode :", error);
      return res
        .status(STATUS_CODE.TOKEN_ERR)
        .send(new ErrorMessage(MESSAGE_CONST.IN_VALID.code, ["Token"]));
    }
  }
};

/*
 * debuging, not in use
 *
 */
exports.isLoggedIn = async (req, res, next) => {
  if (req.cookies.jwt) {
    try {
      // 1) verify token
      const decoded = await promisify(jwt.verify)(
        req.cookies.jwt,
        process.env.JWT_SECRET
      );
      // 2) Check if user still exists
      const currentUser = await User.getOne({ id: decoded.id });
      if (!currentUser) {
        return next();
      }
      // 3) Check if user changed password after the token was issued
      if (currentUser.changedPasswordAfter(decoded.iat)) {
        return next();
      }
      // THERE IS A LOGGED IN USER
      res.locals.user = currentUser;
      return next();
    } catch (err) {
      return next();
    }
  }
  next();
};

/*
 * check role to use this api
 *
 */
exports.restrictTo =
  (...roles) =>
  (req, res, next) => {
    // roles ['ADMIN', 'USER']. role='USER'
    if (!roles.includes(req.user.role)) {
      return res
        .status(STATUS_CODE.AUTH_ERR)
        .send(
          new ErrorMessage(MESSAGE_CONST.NOT_PERMISSION.code, ["truy cập api"])
        );
    }
    next();
  };

exports.getUserToken = async (req) => {
  let token;
  if (
    req.headers.authorization &&
    req.headers.authorization.startsWith("Bearer")
  ) {
    token = req.headers.authorization.split(" ")[1];
  }
  if (!token) return null;
  else {
    const decoded = await jwt.verify(token, process.env.JWT_SECRET);
    return decoded;
  }
};
