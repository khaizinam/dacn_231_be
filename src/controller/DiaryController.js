const { STATUS_CODE } = require("../constants/item.constants");
const { DiaryModel } = require("../models/DiaryModel");
const { getTree } = require("./gardenController");

module.exports.DiaryController = () => {
    const self = {};

    self.get = async function (request, response) {
        try {
            const { tree_id } = request.params;
            const treeInfo = await getTree(tree_id);
            return response.json(treeInfo);
        } catch (error) {
            console.error(error);
            response.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
        }
    };

    self.store = async function (request, response) {
        const { tree_id } = request.params;
        const image_url = request.file?.path || null;
        const { stage_no, plant_id, note, weather, status, date_published } =
            request.body;
        try {
            const valid = await validDate(date_published, tree_id);
            if (valid) {
                const newDiary = {
                    tree_id,
                    stage_no,
                    plant_id,
                    note,
                    weather,
                    status,
                    date_published,
                    image_url,
                };
                const saveRes = await DiaryModel().store(newDiary);
                if (saveRes) {
                    const newData = await getTree(tree_id);
                    response.status(200).json(newData);
                } else {
                    response.status(404).json({ message: "Lưu không thành công!" });
                }
            } else response.status(404).json({ message: "Ngày không hợp lệ!" });
        } catch (error) {
            response
                .status(500)
                .json({ systemLog: error.message, message: "Lỗi hệ thống!" });
        }
    };

    async function validDate(date, tree_id) {
        const isExist = await DiaryModel().filter({
            where: {
                date_published: date,
                tree_id: tree_id
            },
        });
        if (isExist !== null) return false;
        return true;
    }

    self.update = async function (request, response) {
        try {
            const { diary_id } = request.params;
            const image_url = request.body?._img || request.file?.path || null;
            const { note, weather, status } = request.body;
            const newDiary = {
                note,
                weather,
                status,
                image_url,
            };
            const saveRes = await DiaryModel().update_by_id(newDiary, diary_id);
            if (saveRes.length > 0) {
                const _diary = await DiaryModel().find_by_id(diary_id);
                const newData = await getTree(_diary.tree_id);
                response.json(newData);
            } else {
                response.status(404).json("Diary not found!");
            }
        } catch (error) {
            response.status(500).json({ error: error.message });
        }
    };

    self.destroy = async function (request, response) {
        try {
            const { diary_id } = request.params;
            const deleted = await DiaryModel().destroy_by_id(diary_id);
            if (deleted) {
                response.json("Deleted");
            } else {
                response.status(STATUS_CODE.BAD_REQUEST).json("Bad Request");
            }
        } catch (error) {
            console.error(error);
            response.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
        }
    };
    return self;
}
