const { STATUS_CODE } = require("../constants/item.constants");
const { MESSAGE_CONST } = require("../constants/message.constants");
const { ErrorMessage } = require("../models/commonResponse");

const db = require("../db/models/index");
const { Sequelize, Op, fn, col, where } = require("sequelize");
const authController = require("./authController");
const Moment = require("moment");
const { log } = require("util");
/*---------------------------------*/
const { PlantPosts, PlantStage, Plants, PlantFeedBack, User } = db;

const getPlantPosts = async (plantId) => {
  try {
    if (!plantId) {
      res.status(STATUS_CODE.NOT_FOUND).send("Not found that plant");
    } else {
      const data = await PlantPosts.findAll({
        where: { plant_id: plantId },
        order: [["created_at"]],
      });
      let found = [];
      data.forEach((element) => {
        found.push(element.dataValues);
      });
      return found;
    }
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

const getPlantStages = async (plantId) => {
  try {
    if (!plantId) {
      res.status(STATUS_CODE.NOT_FOUND).send("Not found that plant");
    } else {
      const data = await PlantStage.findAll({
        where: { plant_id: plantId },
        order: [["no"]],
      });
      let found = [];
      data.forEach((element) => {
        found.push(element.dataValues);
      });
      return found;
    }
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

exports.getPlant = async (req, res) => {
  try {
    const { plantId } = req.params;
    const plant = await Plants.findOne({ where: { id: plantId } });
    if (plant) {
      const stages = await getPlantStages(plantId);
      const posts = await getPlantPosts(plantId);
      res.json({
        ...plant.dataValues,
        stages,
        posts,
      });
    }
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

// ---------------------------------- STAGE PLANT ---------------------------------

exports.getStage = async (req, res) => {
  try {
    const { plantId } = req.params;
    const stages = await getPlantStages(plantId);
    res.status(STATUS_CODE.OK).json(stages);
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

exports.createStage = async (req, res) => {
  try {
    const { plantId } = req.params;
    let {
      name = "",
      description = "",
      humidity_above = 0,
      humidity_below = 0,
      range = 1,
    } = req.body;

    name = name.trim();
    description = description.trim();

    if (!name) {
      res.status(STATUS_CODE.BAD_REQUEST).json("Chưa có tên giai đoạn");
    } else {
      const numOfStage = await PlantStage.count({
        where: {
          plant_id: plantId,
          [Op.or]: [{ name: { [Op.iLike]: `%${name}%` } }],
        },
      });
      if (numOfStage >= 1) {
        res
          .status(STATUS_CODE.IS_EXISTED)
          .send(
            new ErrorMessage(MESSAGE_CONST.IS_EXISTED.code, ["Tên giai đoạn"])
          );
      } else {
        const no = await PlantStage.max("no", { where: { plant_id: plantId } });
        const created = await PlantStage.upsert({
          no: no + 1 || 1,
          name,
          description,
          humidity_above,
          humidity_below,
          range,
          img_url: req.file?.path,
          plant_id: plantId,
        });
        if (created) {
          const stages = await getPlantStages(plantId);
          res.status(STATUS_CODE.OK).json(stages);
        } else {
          res.status(STATUS_CODE.BAD_REQUEST).json("BAD_REQUEST");
        }
      }
    }
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

exports.updateStage = async (req, res) => {
  try {
    const { stageId } = req.params;
    let dataUpdate = req.body;
    const stage = await PlantStage.findByPk(stageId);
    const count = await PlantStage.count({
      where: {
        plant_id: stage?.plant_id,
        id: {
          [Op.ne]: stageId,
        },
        [Op.or]: [{ name: { [Op.iLike]: `%${dataUpdate?.name}%` } }],
      },
    });
    if (count >= 1) {
      res
        .status(STATUS_CODE.IS_EXISTED)
        .send(
          new ErrorMessage(MESSAGE_CONST.IS_EXISTED.code, ["Tên giai đoạn"])
        );
    } else {
      if (req.file?.path) dataUpdate.img_url = req.file?.path;
      const update = await PlantStage.update(dataUpdate, {
        where: { id: stageId },
      });
      if (update) {
        const stages = await getPlantStages(dataUpdate.plant_id);
        res.status(STATUS_CODE.OK).json(stages);
      }
      res.json("sucees");
    }
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

exports.changeOrder = async (req, res) => {
  try {
    const { plantId } = req.params;
    const { oldIndex, newIndex } = req.body;
    const oldPhase = await PlantStage.findOne({
      where: {
        no: oldIndex,
        plant_id: plantId,
      },
    });
    const listRange =
      oldIndex < newIndex
        ? [parseInt(oldIndex) + 1, newIndex - 0]
        : [newIndex - 0, oldIndex - 1];

    const listPhase = await PlantStage.findAll({
      where: {
        no: {
          [Op.between]: listRange,
        },
        plant_id: plantId,
      },
    });

    const promise = await Promise.all(
      listPhase.map((phase) =>
        oldIndex < newIndex
          ? phase.decrement({ no: 1 })
          : phase.increment({ no: 1 })
      )
    );
    if (promise) {
      await oldPhase.update({ no: newIndex });
    }
    res.json(
      await PlantStage.findAll({
        where: {
          plant_id: plantId,
        },
        order: [["no"]],
      })
    );
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

exports.deleteStage = async (req, res) => {
  try {
    const { stageId } = req.params;
    const { plant_id, no } = await PlantStage.findOne({
      where: { id: stageId },
    });
    const deleted = await PlantStage.destroy({ where: { id: stageId } });
    const listPhase = await PlantStage.findAll({
      where: {
        plant_id,
      },
    });
    const promise = await Promise.all(
      listPhase.map((phase, index) => phase.update({ no: index + 1 }))
    );
    if (deleted && promise) {
      res.json(
        await PlantStage.findAll({
          where: {
            plant_id,
          },
          order: [["no"]],
        })
      );
    } else {
      res.status(STATUS_CODE.NOT_FOUND).json("Not Found");
    }
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

// ---------------------------------- POST PLANT ---------------------------------
exports.createPost = async (req, res) => {
  try {
    const { plantId } = req.params;
    const { title, content } = req.body;
    const numOfPost = await PlantPosts.count({
      where: {
        plant_id: plantId,
        [Op.or]: [{ title: { [Op.iLike]: `%${title}%` } }],
      },
    });
    if (numOfPost >= 1) {
      res
        .status(STATUS_CODE.IS_EXISTED)
        .send(new ErrorMessage(MESSAGE_CONST.IS_EXISTED.code, ["Tên tiêu đề"]));
    } else {
      const created_at = new Date().getTime();
      const created = await PlantPosts.upsert({
        plant_id: plantId,
        no: 1,
        title,
        content,
        created_at,
        modified_at: created_at,
      });
      if (created) {
        res.status(STATUS_CODE.OK).json(created[0]);
      } else {
        res.status(STATUS_CODE.BAD_REQUEST).json("BAD_REQUEST");
      }
    }
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

exports.updatePost = async (req, res) => {
  try {
    const { postId } = req.params;
    console.log(postId);
    let dataUPdate = req.body;
    const post = await PlantPosts.findByPk(postId);
    delete dataUPdate["id"];
    delete dataUPdate["modified_at"];
    const count = await PlantPosts.count({
      where: {
        plant_id: post?.plant_id,
        id: {
          [Op.ne]: postId,
        },
        [Op.or]: [{ title: { [Op.iLike]: `%${dataUPdate?.title}%` } }],
      },
    });
    if (count >= 1) {
      res
        .status(STATUS_CODE.IS_EXISTED)
        .send(new ErrorMessage(MESSAGE_CONST.IS_EXISTED.code, ["Tên tiêu đề"]));
    } else {
      const modified_at = new Date().getTime();
      const update = await PlantPosts.update(
        { ...dataUPdate, modified_at },
        { where: { id: postId } }
      );
      res.status(STATUS_CODE.OK).json("done");
    }
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

exports.deletePost = async (req, res) => {
  try {
    const { postId } = req.params;
    const deleted = await PlantPosts.destroy({ where: { id: postId } });
    res.status(STATUS_CODE.OK).json("done");
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

exports.updateDescription = async (req, res) => {
  try {
    const { plantId } = req.params;
    const { description } = req.body;

    const update = await Plants.update(
      { description },
      { where: { id: plantId } }
    );
    res.status(STATUS_CODE.OK).json("done");
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

exports.getPlantsList = async (req, res) => {
  try {
    const typeOfTree = req.query.type !== "" ? req.query.type : "";
    const limit = req.query.limit !== "" ? req.query.limit : 20;
    const offset = req.query.offset !== "" ? req.query.offset : 0;
    let searchTerm = "";
    if (req.query.searchTerm) searchTerm = req.query.searchTerm;
    let plants = "";
    let category = "";
    let total = 0;
    const getQueryPlantsByType = (typeOfTree, searchTerm) => ({
      where: {
        category: typeOfTree,
        // [Op.or]: [
        //   { name: { [Op.iLike]: `%${searchTerm}%` } },
        //   { description: { [Op.iLike]: `%${searchTerm}%` } },
        // ],
        [Op.or]: [
          where(fn("unaccent", col("name")), {
            [Op.iLike]: fn("unaccent", `%${searchTerm}%`),
          }),
          where(fn("unaccent", col("description")), {
            [Op.iLike]: fn("unaccent", `%${searchTerm}%`),
          }),
          where(fn("unaccent", col("category")), {
            [Op.iLike]: fn("unaccent", `%${searchTerm}%`),
          }),
        ],
      },
      raw: "true",
      nest: true,
    });

    const getQueryAllPlants = (searchTerm) => ({
      order: [
        ["id", "ASC"], // Sort ascending by 'id' field
      ],
      where: {
        // [Op.or]: [
        //   { name: { [Op.iLike]: `%${searchTerm}%` } },
        //   { description: { [Op.iLike]: `%${searchTerm}%` } },
        // ],
        [Op.or]: [
          where(fn("unaccent", col("name")), {
            [Op.iLike]: fn("unaccent", `%${searchTerm}%`),
          }),
          where(fn("unaccent", col("description")), {
            [Op.iLike]: fn("unaccent", `%${searchTerm}%`),
          }),
          where(fn("unaccent", col("category")), {
            [Op.iLike]: fn("unaccent", `%${searchTerm}%`),
          }),
        ],
      },

      raw: "true",
      nest: true,
    });

    if (typeOfTree) {
      // Get by type
      category = typeOfTree;
      const queryPlantsByType = getQueryPlantsByType(typeOfTree, searchTerm);
      total = await Plants.count({
        ...queryPlantsByType,
      });
      plants = await Plants.findAll({
        ...queryPlantsByType,
        offset: offset,
        limit: limit,
      });
    } else {
      // Get all
      const queryAllPlants = getQueryAllPlants(searchTerm);
      total = await Plants.count({
        ...queryAllPlants,
      });
      plants = await Plants.findAll({
        ...queryAllPlants,
        limit: limit,
        offset: offset,
      });
    }
    res.json({ category: category, total: total, plants });
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

exports.getCategoryList = async (req, res) => {
  try {
    const categoryList = await Plants.findAll({
      attributes: [
        [Sequelize.fn("DISTINCT", Sequelize.col("category")), "category"],
      ],
      raw: true,
    });
    res.json({ categoryList });
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

exports.createPlant = async (req, res) => {
  try {
    const inputArr = ["name", "category", "description"];
    let isValid = true;
    const data = req.body;
    if (!req.file || typeof req.file === "undefined") isValid = false;
    for (let ele of inputArr) {
      if (!data[ele] || typeof data[ele] === "undefined") isValid = false;
      break;
    }
    if (!isValid) {
      res
        .status(STATUS_CODE.NOT_FOUND)
        .send(
          new ErrorMessage(MESSAGE_CONST.NOT_FOUND.code, ["input parameter"])
        );
    } else {
      const isExist = await Plants.findOne({
        where: {
          // name: data.name
          [Op.or]: [{ name: { [Op.iLike]: `%${data?.name}%` } }],
        },
      });
      if (isExist) {
        res
          .status(STATUS_CODE.IS_EXISTED)
          .send(
            new ErrorMessage(MESSAGE_CONST.IS_EXISTED.code, ["Tên cây trồng"])
          );
      } else {
        const date = new Date();
        const created = await Plants.create({
          name: data.name,
          avatar: req.file.path,
          description: data.description,
          category: data.category,
          updated_at: date.toISOString(),
        });
        if (created) {
          res.status(STATUS_CODE.OK).json(created);
        } else res.status(STATUS_CODE.BAD_REQUEST).json("BAD_REQUEST");
      }
    }
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

exports.updatePlant = async (req, res) => {
  try {
    const { plantId } = req.params;
    let isValid = true;
    const data = req.body;
    if (!plantId || typeof plantId === "undefined") isValid = false;
    if (!isValid) {
      res
        .status(STATUS_CODE.NOT_FOUND)
        .send(
          new ErrorMessage(MESSAGE_CONST.NOT_FOUND.code, ["input parameter"])
        );
    } else {
      const count = await Plants.count({
        where: {
          id: {
            [Op.ne]: plantId,
          },
          // name: data?.name
          [Op.or]: [{ name: { [Op.iLike]: `%${data?.name}%` } }],
        },
      });
      if (count >= 1) {
        res
          .status(STATUS_CODE.IS_EXISTED)
          .send(
            new ErrorMessage(MESSAGE_CONST.IS_EXISTED.code, ["Tên cây trồng"])
          );
      } else {
        const objPlant = {};
        const attributeArr = ["name", "description", "category"];
        for (let ele of attributeArr) {
          if (data[ele] && typeof data[ele] === "string")
            objPlant[ele] = data[ele];
        }
        if (req.file && typeof req.file !== "undefined")
          objPlant["avatar"] = req.file.path;
        const created = await Plants.update(objPlant, {
          where: { id: plantId },
        });
        if (created) {
          res.status(STATUS_CODE.OK).json(created);
        } else res.status(STATUS_CODE.BAD_REQUEST).json("BAD_REQUEST");
      }
    }
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

exports.storeFeedBack = async (req, res) => {
  try {
    const { plant_id, content } = req.body;

    const user = await authController.getUserToken(req);
    const created_at = new Date().toISOString();
    const created = await PlantFeedBack.upsert({
      plant_id: plant_id,
      author_id: user.id,
      is_seen: false,
      time_seen: null,
      content,
      created_at,
    });
    if (created) {
      res.status(STATUS_CODE.OK).json(created[0]);
    } else {
      res.status(STATUS_CODE.BAD_REQUEST).json("BAD_REQUEST");
    }
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

exports.deletePlant = async (req, res) => {
  try {
    if (!req.params.plantId) {
      res
        .status(STATUS_CODE.NOT_FOUND)
        .send(new ErrorMessage(MESSAGE_CONST.NOT_FOUND.code, ["plantId"]));
    } else {
      const { plantId } = req.params;
      currPlant = await Plants.findOne({
        where: {
          id: plantId,
        },
      });
      if (!currPlant) {
        res
          .status(STATUS_CODE.NOT_FOUND)
          .send(new ErrorMessage(MESSAGE_CONST.IN_VALID.code, ["plantId"]));
      } else {
        await Plants.destroy({
          where: {
            id: plantId,
          },
        });
        res.status(STATUS_CODE.OK).json("done");
      }
    }
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

exports.getFeedBackByFilter = async (req, res) => {
  try {
    const { queries, order, page, q } = req.body;

    // const user = await authController.getUserToken(req);
    let _where = queries ? queries : {};
    if (q && q !== "") {
      _where = {
        ..._where,
        [Op.or]: [{ id: parseInt(q) }],
      };
    }
    const _limit = 20;
    const data = await PlantFeedBack.findAll({
      where: _where,
      order: order ? order : ["id"],
      limit: _limit,
      offset: page ? (page - 1) * _limit : 0,
    });
    const totalItem = await PlantFeedBack.count({
      where: _where,
    });

    const totalPage = Math.ceil(totalItem / _limit);
    const found = [];

    for (let element of data) {
      const user = await User.findOne({
        where: { id: element.author_id },
      });
      const plant = await Plants.findOne({ where: { id: element.plant_id } });
      if (plant && user) {
        found.push({
          ...element.dataValues,
          author_name: user.full_name,
          plant_avatar: plant.avatar,
          plant_name: plant.name,
          plant_id: plant.id,
          author_id: user.id,
          is_seen: element.dataValues["is_seen"] ? "Đã xem" : "Đang chờ",
          created_at: Moment(element.dataValues["created_at"]).format(
            "YYYY/MM/DD, HH:mm"
          ),
          time_seen: element.dataValues["time_seen"]
            ? Moment(element.dataValues["time_seen"]).format(
                "YYYY/MM/DD, HH:mm"
              )
            : null,
        });
      }
    }
    res.status(STATUS_CODE.OK).json({
      items: found,
      page: page,
      total_page: totalPage,
    });
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

exports.updateFeedBack = async (req, res) => {
  try {
    const { where, values } = req.body;

    // const user = await authController.getUserToken(req);
    const time_seen = Moment(new Date().getTime()).format(
      "YYYY/MM/DD, H:mm:ss"
    );

    const data = await PlantFeedBack.update(
      { ...values, time_seen: time_seen },
      {
        where: where,
      }
    );

    res.status(STATUS_CODE.OK).json({ time_seen: time_seen });
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

exports.seenFeedBack = async (req, res) => {
  try {
    const { id } = req.params;
    const time_seen = Moment().format("YYYY/MM/DD, H:mm:ss");
    const data = await PlantFeedBack.update(
      { is_seen: true, time_seen },
      { where: { id } }
    );
    res.json({ time_seen });
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Lỗi hệ thống");
  }
};
