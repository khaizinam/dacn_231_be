const { STATUS_CODE } = require("../constants/item.constants");
const { MESSAGE_CONST } = require("../constants/message.constants");
const { ErrorMessage } = require("../models/commonResponse");

const db = require("../db/models/index");
const { Sequelize, Op } = require("sequelize");
const authController = require("./authController");
const Moment = require("moment");
const { DiaryModel } = require("../models/DiaryModel");
/*---------------------------------*/
const { Tree, TreeDiary, User, PlantFeedBack } = db;

const contentStatusMonth = ['labels', 'Khoẻ mạnh', 'Bị bệnh', 'Thu hoạch', 'Đã chết']
const colorStatusMonth = ['#4caf50','#ffb300','#2196f3','#f44336']
const contentStatusYear = ['labels', 'Đang trồng', 'Không còn']
const themeStatusYear = ['#4caf50', '#f44336']

const contentNewSubscriberYear = ['labels', 'Người đang ký mới']
const contentTreeYear = ['labels', 'Cây trồng mới']

const iconColor = ['#008000', '#FFFF00', '#0000FF', '#FF0000']
const statusName = ['Khoẻ mạnh', 'Bị bệnh', 'Thu hoạch', 'Đã chết'];

const generalStatisticName = ['Người đăng ký', 'Phản hồi',"Cây trồng"];

const CONST_THEME = [
  {
    bg_color:'#4caf50',
    icon_color : '#008000',
    label : 'Số người đang trồng'
  },
  {
    bg_color:'#4caf50',
    icon_color : '#008000',
    label : 'Khoẻ mạnh',
  },
  {
    bg_color:'#ffb300',
    icon_color : '#FFFF00',
    label : 'Bị bệnh',
  },
  {
    bg_color:'#2196f3',
    icon_color : '#0000FF',
    label : 'Thu hoạch',
  },
  {
    bg_color:'#FF0000',
    icon_color : 'white',
    label : 'Đã chết',
  }
 
];

let handleStatusClassifyYear = (data, indexMonth, indexYear) => {
  let result = [indexMonth,0,0]
  data.forEach((ele) => {
    let dataRow = ele?.dataValues?.diaries[0]?.dataValues;
    if (dataRow) {
      if (dataRow?.status == 2 || dataRow?.status == 3) {
        if (parseInt(dataRow?.date_published.slice(5, 7)) == indexMonth && parseInt(dataRow?.date_published.slice(0, 4)) == indexYear) {
          result[2] += 1
        }
      }
      else {
        result[1] += 1
      }
    }
  });
  return result;
}

let handleClassifyStatusMonth = (data, indexDate) => {
  let result = [indexDate.getDate(),0,0,0,0]
  data.forEach((ele, index) => {
    let dataRow = ele?.dataValues?.diaries[0]?.dataValues;
    if (dataRow) {
      if (dataRow?.status == 2 || dataRow?.status == 3) {
        if (dataRow?.date_published == indexDate.toISOString().slice(0, 10)) {
          result[dataRow?.status + 1] += 1
        }
      }
      else {
        result[dataRow?.status + 1] += 1
      }
    }
  });
  return result;
}

exports.getStatusOfTreeData = async (req, res) => {
  try {
    const inputArr = ["year"];
    let isValid = true;
    const inputData = req.query;
    for (let ele of inputArr) {
      if (!inputData[ele] || typeof inputData[ele] === "undefined") {
        isValid = false;
        break;
      }
      // else if (ele === "option" && inputData[ele] === "month"
      // && (!inputData["month"] || typeof inputData["month"] === "undefined")) {
      //   isValid = false;
      //   break;
      // }
    }
    if (!isValid) {
      res
        .status(STATUS_CODE.NOT_FOUND)
        .send(
          new ErrorMessage(MESSAGE_CONST.NOT_FOUND.code, ["input parameter"])
        );
    }
    else {
      let arrResult = [];
      let theme = []
      let currentDate = new Date();
      currentDate.setUTCHours(23, 59, 59, 0);
      const currentYear = currentDate.getFullYear();
      const currentMonth = currentDate.getMonth() + 1;
      let option = "";
      if (!inputData["month"] || typeof inputData["month"] === "undefined") {
        option = "year"
      }
      else {
        option = "month"
      }
      
      if (option === "month") {
        arrResult.push(contentStatusMonth);
        theme = colorStatusMonth.slice();
        // Khởi tạo một đối tượng Date với ngày đầu tiên của tháng
        let indexDate = new Date();
        indexDate.setUTCHours(0, 0, 0, 0);
        indexDate.setDate(1);
        indexDate.setMonth(inputData?.month - 1);
        indexDate.setFullYear(inputData?.year)
        // Lặp qua từng ngày trong tháng
        while (indexDate.getMonth() === inputData?.month - 1 && indexDate <= currentDate) { // (chú ý: tháng trong JavaScript bắt đầu từ 0)
          // Hiển thị ngày hiện tại
          // console.log(indexDate.toISOString().slice(0, 10)); // Format ngày dưới dạng YYYY-MM-DD
          let data = await Tree.findAll({
            attributes: ['id'],
            include: [
              {
                model: db.TreeDiary, as: 'diaries',
                where: {
                  date_published: {
                    [Sequelize.Op.lte]: indexDate
                  }
                },
                order: [['date_published', 'DESC']],
                limit: 1,
                nest: true
              },
            ],
            nest: true
          })
          arrResult.push(handleClassifyStatusMonth(data, indexDate))
          // Tăng ngày lên 1
          indexDate.setDate(indexDate.getDate() + 1);
        }
      }
      else if (option === "year") {
        if (inputData?.year > currentYear) {
          res
            .status(STATUS_CODE.BAD_REQUEST)
            .send(
              new ErrorMessage(MESSAGE_CONST.IN_VALID.code, ["year"])
              );
          return;
        }
        arrResult.push(contentStatusYear);
        theme = themeStatusYear.slice();
        let maxMonth = 12;
        if (inputData?.year == currentYear) {
          maxMonth = currentMonth
        }
        let indexMonth = 1;

        // Lặp qua từng tháng trong năm
        while (indexMonth <= maxMonth) { 
          let finalDateInMonth = ''
          //////////////////////////////////
          if (indexMonth == currentMonth && inputData?.year == currentMonth) {
            finalDateInMonth = new Date(`${currentYear}-${currentMonth}-${currentDate.getDate()}`)
          }
          /////////////////////////////////
          else if (indexMonth == 12) {
            finalDateInMonth = new Date(`${inputData?.year}-12-31`);
          }
          else {
            finalDateInMonth = new Date(`${inputData?.year}-${indexMonth + 1}-01`);
            finalDateInMonth.setDate(finalDateInMonth.getDate() - 1);
          }
          let data = await Tree.findAll({
            attributes: ['id'],
            include: [
              {
                model: db.TreeDiary, as: 'diaries',
                where: {
                  date_published: {
                    [Sequelize.Op.lte]: finalDateInMonth
                  }
                },
                order: [['date_published', 'DESC']],
                limit: 1,
                nest: true
              },
            ],
            nest: true
          })
          arrResult.push(handleStatusClassifyYear(data, indexMonth, inputData?.year))
          indexMonth += 1
        }
      }
      res.json({ theme: theme, data: arrResult } );
    }
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

exports.getFirstDiary = async (req, res) => {
  try {
    const inputData = req.query;
      const earliestDiary = await TreeDiary.findOne({
        attributes: ['date_published'],
        include: [
          {
            model: Tree,
            as: 'tree', 
            attributes: []
          }
        ],
        order: [['date_published', 'ASC']],
        // order: [['date_published', 'DESC']]
      });     
      res.json( earliestDiary );
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

let countStatus = (data) => {
  let result = statusName.map((ele, index) => {
    return {
      name_status: ele,
      status_code: index,
      bgColor: colorStatusMonth[index],
      iconColor: iconColor[index],
      count: 0
    }
  })
  data.forEach((ele, index) => {
    let treeDiaryArr = ele?.dataValues?.diaries
    if (treeDiaryArr?.length === 0) {
      // result[0]['count'] += 1
    }
    else {
      let status_code = treeDiaryArr[0]?.dataValues?.status;
      result[status_code]['count'] += 1
    }
  });
  return result;
}

exports.getNumberTreeStatuses = async (req, res) => {
  try {
    let data = await Tree.findAll({
      attributes: ['id'],
      include: [
        {
          model: db.TreeDiary, as: 'diaries',
          attributes: ['status'],
          where: {
            date_published: {
              [Sequelize.Op.lte]: new Date()
            }
          },
          order: [['date_published', 'DESC']],
          limit: 1
        },
      ]
    })
    res.json( countStatus(data) );
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};


function genResult (d=[0,1]) {
  const r = CONST_THEME.map(ele => ({ 'name_status' : ele.label,'bgColor': ele.bg_color,'iconColor': ele.icon_color,'count': 0}) );
  d.forEach((e,i) => r[i].count = e);
  return r;
}

exports.getNumberPlants = async (req, res) => {
  try {
    const {plant_id} = req?.params;
    const d = await DiaryModel().get_num_by_plant_id(plant_id);
    res.json( genResult(d) );
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

let handleNewSubscriberStatisticalData = async (year) => {
  let arrDataResult = [];
  let theme = []
  arrDataResult.push(contentNewSubscriberYear);
  theme = themeStatusYear.slice();
  const currentDate = new Date();
  const currentYear = currentDate.getFullYear();
  const currentMonth = currentDate.getMonth() + 1; // Tháng hiện tại (0-11)



  if (year > currentYear) {
    res
      .status(STATUS_CODE.BAD_REQUEST)
      .send(
        new ErrorMessage(MESSAGE_CONST.IN_VALID.code, ["year"])
        );
    return;
  }

  const maxMonth = (year == currentYear) ? currentMonth : 12;

  for (let indexMonth = 1; indexMonth <= maxMonth; indexMonth++) {
    const startDate = new Date(year, indexMonth - 1, 1); // Ngày đầu tiên của tháng
    const endDate = new Date(year, indexMonth, 1);       // Ngày cuối cùng của tháng
    if (year == currentYear && indexMonth === currentMonth) {
      endDate.setDate(currentDate.getDate() + 1); // Sửa ngày cuối cùng của tháng thành ngày hiện tại
    }
    const numNewSub = await User.count({
      where: {
        role: "USER",
        create_at: {
          [Op.and]: [
            { [Op.gt]: startDate }, // Greater than or equal to start date of the month
            { [Op.lte]: endDate }      // Less than or equal to end date of the month
          ]
        }
      },
      raw: true
    });
    arrDataResult.push([indexMonth, numNewSub]);
  }
  return {
    theme: theme,
    data: arrDataResult
  }
}

exports.getNewSubscriberStatisticalData = async (req, res) => {
  try {
    const inputArr = ["year"];
    let isValid = true;
    const inputData = req.query;
    for (let ele of inputArr) {
      if (!(inputData[ele] ?? false)) {
        isValid = false;
        break;
      }
    }
    if (!isValid) {
      res
        .status(STATUS_CODE.NOT_FOUND)
        .send(
          new ErrorMessage(MESSAGE_CONST.NOT_FOUND.code, ["input parameter"])
        );
    }
    else {
      const data = await handleNewSubscriberStatisticalData(inputData?.year);
      res.json( data );
    }
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

exports.getFirstUserRegisterTime = async (req, res) => {
  try {
    const earliestDiary = await User.findOne({
      attributes: ['create_at'],
      where: {
        role: "USER"
      },
      order: [['create_at', 'ASC']],
      raw: true
    });     
      res.json( earliestDiary?.create_at );
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

let handleGetGeneralStatisticData = async () => {
  try {
    const today = new Date();
    countResultArr = []
    // today.setHours(0, 0, 0, 0); // Đặt thời gian về đầu ngày
      const totalUserCount = await User.count({
        where: {
          role: 'USER'
        }
      });

      // Lấy số hàng tăng mới trong ngày có role="USER"
      const newUserCountToday = await User.count({
        where: {
          role: 'USER',
          create_at: {
            [Op.gte]: today // Lấy các hàng có createdAt lớn hơn hoặc bằng thời điểm bắt đầu của ngày hôm nay
          },
        }
      });
      countResultArr.push([totalUserCount, newUserCountToday])
      const totalPlantFedbackCount = await PlantFeedBack.count({});

      // Lấy số hàng tăng mới trong ngày có role="USER"
      const newPlantFedbackCountToday = await PlantFeedBack.count({
        where: {
          created_at: {
            [Op.gte]: today // Lấy các hàng có createdAt lớn hơn hoặc bằng thời điểm bắt đầu của ngày hôm nay
          },
        }
      });
    countResultArr.push([totalPlantFedbackCount, newPlantFedbackCountToday])

    const totalTreeCount = await Tree.count({});
    const newTreeCountToday = await Tree.count({
      where: {
        created_at: {
          [Op.gte]: today // Lấy các hàng có createdAt lớn hơn hoặc bằng thời điểm bắt đầu của ngày hôm nay
        },
      }
    });
    countResultArr.push([totalTreeCount, newTreeCountToday])

    let result = generalStatisticName.map((ele, index) => {
      return {
        name: ele,
        bgColor: colorStatusMonth[index],
        count: countResultArr[index][0],
        increase_in_day: countResultArr[index][1]
      }
    })
    return result
  } catch (error) {
    console.error(error);
    return false;
  }
}

// Get the number of subscribers, number of plants, number of feedback
exports.getGeneralStatisticData = async (req, res) => {
  try {
    const returnData = await handleGetGeneralStatisticData();
    if (!returnData) res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
    else res.json( returnData );
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

let handleGetTreeStatisticData = async (year) => {
  let arrDataResult = [];
  let theme = []
  arrDataResult.push(contentTreeYear);
  theme = themeStatusYear.slice();
  const currentDate = new Date();
  const currentYear = currentDate.getFullYear();
  const currentMonth = currentDate.getMonth() + 1; // Tháng hiện tại (0-11)



  if (year > currentYear) {
    res
      .status(STATUS_CODE.BAD_REQUEST)
      .send(
        new ErrorMessage(MESSAGE_CONST.IN_VALID.code, ["year"])
        );
    return;
  }

  const maxMonth = (year == currentYear) ? currentMonth : 12;

  for (let indexMonth = 1; indexMonth <= maxMonth; indexMonth++) {
    const startDate = new Date(year, indexMonth - 1, 1); // Ngày đầu tiên của tháng
    const endDate = new Date(year, indexMonth, 1);       // Ngày cuối cùng của tháng
    if (year == currentYear && indexMonth === currentMonth) {
      endDate.setDate(currentDate.getDate() + 1); // Sửa ngày cuối cùng của tháng thành ngày hiện tại
    }
    const numNewTree = await Tree.count({
      where: {
        created_at: {
          [Op.and]: [
            { [Op.gt]: startDate }, // Greater than or equal to start date of the month
            { [Op.lte]: endDate }      // Less than or equal to end date of the month
          ]
        }
      },
      raw: true
    });
    arrDataResult.push([indexMonth, numNewTree]);
  }
  return {
    theme: theme,
    data: arrDataResult
  }
}

exports.getTreeStatisticalData = async (req, res) => {
  try {
    const inputArr = ["year"];
    let isValid = true;
    const inputData = req.query;
    for (let ele of inputArr) {
      if (!(inputData[ele] ?? false)) {
        isValid = false;
        break;
      }
    }
    if (!isValid) {
      res
        .status(STATUS_CODE.NOT_FOUND)
        .send(
          new ErrorMessage(MESSAGE_CONST.NOT_FOUND.code, ["input parameter"])
        );
    }
    else {
      const data = await handleGetTreeStatisticData(inputData?.year);
      res.json( data );
    }
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

exports.getFirstTreeCreateTime = async (req, res) => {
  try {
    const earliestDiary = await Tree.findOne({
      attributes: ['created_at'],
      order: [['created_at', 'ASC']],
      raw: true
    });     
      res.json( earliestDiary?.created_at );
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};