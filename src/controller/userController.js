const { STATUS_CODE } = require("../constants/item.constants");
const { ResponseModel } = require("../models/commonResponse");
const formatString = require("../feature/formatString");
const db = require("../db/models/index");
/*---------------------------------*/
const User = db.User;

exports.getUsers = async (req, res, next) => {
  const data = await User.findAll({});
  let users = [];
  data.forEach((element) => {
    //delete element.dataValues["password"];
    users.push(element.dataValues);
  });
  const response = new ResponseModel({ contents: users });
  res.status(STATUS_CODE.OK).send(response);
};

exports.create = async ({ email, password, full_name, create_at }) => {
  const create = await User.create({
    email,
    password,
    full_name,
    roles: JSON.stringify(["USER"]),
    create_at,
  });
  const user = await find({ email, password });
  return user;
};

exports.isExits = async ({ email }) => {
  const num = await User.count({
    where: {
      email,
    },
  });
  return num;
};

const find = async (query) => {
  const found = await User.findOne({
    where: { ...query },
  });
  if (!found) return null;
  const user = found.dataValues;
  return user;
};

exports.getOne = async (query) => {
  return find(query);
};

exports.getCurrent = async (req, res, next) => {
  const user = await find({ id: req.userId });
  const { id, email, full_name, avatar, role } = user;
  return res.json({ id, email, full_name, avatar, role });
};

exports.updateProfile = async (req, res) => {
  try {
    const id = req.userId;
    let { full_name, phone_number, location } = req.body;
    full_name = full_name.trim();
    phone_number = phone_number.trim();
    location = location.trim();
    const user = await User.findByPk(id);
    if (!user)
      return res.status(STATUS_CODE.NOT_FOUND).json("Không tìm thấy tài khoản");
    const r = await user.update({ full_name, phone_number, location });
    if (r) {
      res.json(user);
    }
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

exports.changePassword = async (req, res) => {
  try {
    const { oldPassword, newPassword } = req.body;
    const user = await User.findByPk(req.userId);
    if (!user)
      return res.status(STATUS_CODE.NOT_FOUND).json("Không tìm thấy tài khoản");
    if (user.password !== oldPassword)
      return res.status(STATUS_CODE.BAD_REQUEST).json("Mật khẩu cũ không đúng");
    else {
      await user.update({ password: newPassword });
    }
    res.json("successs");
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};
