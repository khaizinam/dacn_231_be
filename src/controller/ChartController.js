const { STATUS_CODE } = require('../constants/item.constants');
const { MESSAGE_CONST } = require('../constants/message.constants');
const { DataTable } = require('../models/DataTable');
const { TreeModal } = require("../models/TreeModal");
const db = require("../db/models/index");
const { Tree, TreeDiary } = db;
const { Sequelize, Op } = require("sequelize");
/*---------------------------------*/

const BASE_FILTER_DAY_OF_MONTH = 0;
const BASE_FILTER_MONTH_OF_YEAR = 1;

const COLOR_HEALTHY = '#4caf50';
const COLOR_SICK = '#ffb300';
const COLOR_HARVEST = '#2196f3';
const COLOR_DEATH = '#f44336';
const TREE_STATUS = {
    HEALTHY: 0,
    SICK: 1,
    HARVEST: 2,
    DEATH: 3
}
const THEMES = [
    [COLOR_HEALTHY, COLOR_SICK, COLOR_HARVEST, COLOR_DEATH],
    [COLOR_HEALTHY, COLOR_DEATH],
];

const LABEl = [
    ['Khoẻ mạnh', 'Bị bệnh', 'Thu hoạch', 'Đã chết'],
    ['Còn sống', 'Đã chết']
];
const QUERY_MONTH = 'month';
const QUERY_YEAR = 'year';
/*----------------------------------------------------------*/
const contentMonth = ['labels', 'Khoẻ mạnh', 'Bị bệnh', 'Thu hoạch', 'Đã chết']
const themeMonth = ['#4caf50','#ffb300','#2196f3','#f44336']
const contentYear = ['labels', 'Đang trồng', 'Không còn']
const themeYear = ['#4caf50', '#f44336']

const iconColor = ['#008000', '#FFFF00', '#0000FF', '#FF0000']
const statusName = ['Khoẻ mạnh', 'Bị bệnh', 'Thu hoạch', 'Đã chết']
/*----------------------------------------------------------*/

module.exports.ChartController = () => {
    const self = {};

    // public
    // self.getFilter = async function (request, response) {
    //     try {
    //         const filterType = (request.body?.month !== "")
    //             ? BASE_FILTER_DAY_OF_MONTH
    //             : BASE_FILTER_MONTH_OF_YEAR;

    //         const { month, year, areas } = request.body;
    //         let data = [];

    //         /* DATE OF MONTH  */
    //         if (filterType === BASE_FILTER_DAY_OF_MONTH) {
    //             data = await initDataTableDateOfMonth({ filterType, month, year, areas, user_id: request.userId });
    //         } else {
    //             if (year > getCurrentYear()) {
    //                 return response
    //                     .status(404)
    //                     .json({ message: "Year much be less than this year!" });
    //             }

    //             data = await initDataTableMonthOfYear({ filterType, year, areas, user_id: request.userId });
    //         }

    //         return response.json({
    //             themes: THEMES[filterType],
    //             title: "Chart table",
    //             num_columns: data.numCols,
    //             num_rows: data.numRows,
    //             data: data.data,
    //         });

    //     } catch (error) {
    //         console.log(error);
    //         return response
    //             .status(500)
    //             .json({ message: error.message });
    //     }
    // }

    // // private
    // async function initDataTableDateOfMonth({ filterType = 0, month = "", year = "", areas = [], user_id = 0 }) {
    //     const table = DataTable(LABEl[filterType]);

    //     const currentDate = new Date(); // prevent init after current date
    //     currentDate.setUTCHours(23, 59, 59, 0);

    //     const flag_date = new Date(); // setup flag date start at day 1 of month
    //     flag_date.setUTCHours(0, 0, 0, 0);
    //     flag_date.setDate(1);
    //     flag_date.setMonth(parseInt(month) - 1);
    //     flag_date.setFullYear(parseInt(year));

    //     const numDateInMonth = daysInMonth(year, month);
    //     for (let index = 1; index <= numDateInMonth; index++) {

    //         const data = await TreeModal().get_by_user_area_date({
    //             user_id,
    //             areas,
    //             date_search: flag_date
    //         });

    //         table.add(index.toString(), classifyData(data, flag_date)); // add data to new row

    //         flag_date.setDate(flag_date.getDate() + 1); // next date
    //     }
    //     return table.get();
    // }

    // // private
    // async function initDataTableMonthOfYear({ filterType = 0, year = "", areas = [], user_id = 0 }) {
    //     const table = DataTable(LABEl[filterType]);

    //     const maxMonth = (year == getCurrentYear()) ? getCurrentMonth() : 12;

    //     for (let month = 1; month <= 12; month++) {
    //         if (month <= maxMonth) {
    //             const finalDateInMonth = getFinaleDateOfMonth(year, month);
    //             const data = await TreeModal().get_by_date_publish(finalDateInMonth, user_id);
    //             table.add(month.toString(), classifyDataYear(data, year)); // add data to new row
    //         }else{
    //             table.add(month.toString(), [0, 0]); // add data to new row
    //         }
    //     }
    //     return table.get();
    // }

    // //private 
    // function classifyDataYear(data, indexYear) {
    //     let result = [0, 0]
    //     data.forEach((item) => {
    //         let dataRow = item.dataValues.diaries[0]?.dataValues;
    //         if (dataRow) {
    //             const status = dataRow.status;

    //             if (status == TREE_STATUS.SICK || status == TREE_STATUS.DEATH) {
    //                 if (parseInt(dataRow?.date_published.slice(5, 7)) == indexMonth
    //                     && parseInt(dataRow?.date_published.slice(0, 4)) == indexYear) {
    //                     result[1] += 1
    //                 }
    //             }
    //             else {
    //                 result[0] += 1
    //             }
    //         }
    //     });
    //     return result;
    // }

    // // private 
    // function getFinaleDateOfMonth(year, indexMonth) {
    //     let finalDateInMonth = ''
    //     let currentDate = new Date();
    //     const currentMonth = getCurrentMonth();
    //     const currentYear = getCurrentYear();
    //     if (indexMonth == currentMonth) {
    //         finalDateInMonth = new Date(`${currentYear}-${currentMonth}-${currentDate.getDate()}`)
    //     }
    //     else if (indexMonth == 12) {
    //         finalDateInMonth = new Date(`${year}-12-31`);
    //     }
    //     else {
    //         finalDateInMonth = new Date(`${year}-${indexMonth + 1}-01`);
    //         finalDateInMonth.setDate(finalDateInMonth.getDate() - 1);
    //     }
    //     return finalDateInMonth;
    // }

    // // private
    // function daysInMonth(month = "1", year = "2024") {
    //     return new Date(parseInt(year), parseInt(month), 0).getDate();
    // }

    // // private
    // function getCurrentYear() {
    //     const date = new Date();
    //     date.setUTCHours(23, 59, 59, 0);
    //     return date.getFullYear();
    // }

    // // private 
    // function getCurrentMonth() {
    //     const date = new Date();
    //     date.setUTCHours(23, 59, 59, 0);
    //     return date.getMonth() + 1;
    // }
    // // private
    // function checkValidCount(status) {
    //     return (status === TREE_STATUS.HARVEST
    //         || status === TREE_STATUS.DEATH);
    // }

    // // private
    // function classifyData(data, indexDate) {
    //     let result = [0, 0, 0, 0]
    //     data.forEach((item) => {
    //         const dataRow = item.dataValues.diaries[0]?.dataValues;
    //         if (dataRow) {
    //             const status = dataRow.status;
    //             if (checkValidCount(status)) {
    //                 if (dataRow.date_published == indexDate.toISOString().slice(0, 10)) {
    //                     result[status] += 1
    //                 }
    //             }
    //             else {
    //                 result[status] += 1;
    //             }
    //         }
    //     });
    //     return result;
    // }

    ////////////////////////////////////////////////////////
    function handleClassifyYear(data, indexMonth, indexYear) {
  let result = [indexMonth,0,0]
  data.forEach((ele) => {
    let dataRow = ele?.dataValues?.diaries[0]?.dataValues;
    if (dataRow) {
      if (dataRow?.status == 2 || dataRow?.status == 3) {
        if (parseInt(dataRow?.date_published.slice(5, 7)) == indexMonth && parseInt(dataRow?.date_published.slice(0, 4)) == indexYear) {
          result[2] += 1
        }
      }
      else {
        result[1] += 1
      }
    }
  });
  return result;
}

function handleClassifyMonth(data, indexDate) {
  let result = [indexDate.getDate(),0,0,0,0]
  data.forEach((ele, index) => {
    let dataRow = ele?.dataValues?.diaries[0]?.dataValues;
    if (dataRow) {
      if (dataRow?.status == 2 || dataRow?.status == 3) {
        if (dataRow?.date_published == indexDate.toISOString().slice(0, 10)) {
          result[dataRow?.status + 1] += 1
        }
      }
      else {
        result[dataRow?.status + 1] += 1
      }
    }
  });
  return result;
}

self.getStatusOfTreeData = async (req, res) => {
  try {
    const inputArr = ["year"];
    let isValid = true;
    const inputData = req.query;
    for (let ele of inputArr) {
      if (!inputData[ele] || typeof inputData[ele] === "undefined") {
        isValid = false;
        break;
      }
      // else if (ele === "option" && inputData[ele] === "month"
      // && (!inputData["month"] || typeof inputData["month"] === "undefined")) {
      //   isValid = false;
      //   break;
      // }
    }
    if (!isValid) {
      res
        .status(STATUS_CODE.NOT_FOUND)
        .send(
          new ErrorMessage(MESSAGE_CONST.NOT_FOUND.code, ["input parameter"])
        );
    }
    else {
      let arrResult = [];
      let theme = []
      let currentDate = new Date();
      currentDate.setUTCHours(23, 59, 59, 0);
      const currentYear = currentDate.getFullYear();
      const currentMonth = currentDate.getMonth() + 1;
      let option = "";
      if (!inputData["month"] || typeof inputData["month"] === "undefined") {
        option = "year"
      }
      else {
        option = "month"
      }
      
      if (option === "month") {
        arrResult.push(contentMonth);
        theme = themeMonth.slice();
        // Khởi tạo một đối tượng Date với ngày đầu tiên của tháng
        let indexDate = new Date();
        indexDate.setUTCHours(0, 0, 0, 0);
        indexDate.setDate(1);
        indexDate.setMonth(inputData?.month - 1);
        indexDate.setFullYear(inputData?.year)
        // Lặp qua từng ngày trong tháng
        while (indexDate.getMonth() === inputData?.month - 1 && indexDate <= currentDate) { // (chú ý: tháng trong JavaScript bắt đầu từ 0)
          // Hiển thị ngày hiện tại
          // console.log(indexDate.toISOString().slice(0, 10)); // Format ngày dưới dạng YYYY-MM-DD
          let data =(inputData?.area_id || inputData?.other_area) ? await Tree.findAll({
            attributes: ['id'],
            where: {
              user_id: req?.userId,
              // ...(inputData?.area_id ? { area_id: inputData?.area_id } : {}),
              [Op.or]: [
                { ...(inputData?.area_id && inputData?.area_id?.length > 0 ? { area_id: inputData?.area_id } : {}) },
                { ...(inputData?.other_area ? { area_id: null } : {}) },
              ]
            },
            include: [
              {
                model: db.TreeDiary, as: 'diaries',
                where: {
                  date_published: {
                    [Sequelize.Op.lte]: indexDate
                  }
                },
                order: [['date_published', 'DESC']],
                limit: 1,
                nest: true
              },
            ],
            nest: true
          }) : []
          arrResult.push(handleClassifyMonth(data, indexDate))
          // Tăng ngày lên 1
          indexDate.setDate(indexDate.getDate() + 1);
        }
      }
      else if (option === "year") {
        if (inputData?.year > currentYear) {
          res
            .status(STATUS_CODE.BAD_REQUEST)
            .send(
              new ErrorMessage(MESSAGE_CONST.IN_VALID.code, ["year"])
              );
          return;
        }
        arrResult.push(contentYear);
        theme = themeYear.slice();
        let maxMonth = 12;
        if (inputData?.year == currentYear) {
          maxMonth = currentMonth
        }
        let indexMonth = 1;

        // Lặp qua từng tháng trong năm
        while (indexMonth <= maxMonth) { 
          let finalDateInMonth = ''
          //////////////////////////////////
          if (indexMonth == currentMonth && inputData?.year == currentMonth) {
            finalDateInMonth = new Date(`${currentYear}-${currentMonth}-${currentDate.getDate()}`)
          }
          /////////////////////////////////
          else if (indexMonth == 12) {
            finalDateInMonth = new Date(`${inputData?.year}-12-31`);
          }
          else {
            finalDateInMonth = new Date(`${inputData?.year}-${indexMonth + 1}-01`);
            finalDateInMonth.setDate(finalDateInMonth.getDate() - 1);
          }
          let data =(inputData?.area_id || inputData?.other_area) ? await Tree.findAll({
            attributes: ['id'],
            where: {
              user_id: req?.userId,
              // ...(inputData?.area_id ? { area_id: inputData?.area_id } : {})
              [Op.or]: [
                { ...(inputData?.area_id && inputData?.area_id?.length > 0 ? { area_id: inputData?.area_id } : {}) },
                { ...(inputData?.other_area ? { area_id: null } : {}) },
              ]
            },
            include: [
              {
                model: db.TreeDiary, as: 'diaries',
                where: {
                  date_published: {
                    [Sequelize.Op.lte]: finalDateInMonth
                  }
                },
                order: [['date_published', 'DESC']],
                limit: 1,
                nest: true
              },
            ],
            nest: true
          }) : []
          arrResult.push(handleClassifyYear(data, indexMonth, inputData?.year))
          indexMonth += 1
        }
      }
      res.json({ theme: theme, data: arrResult } );
    }
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

self.getFirstDiary = async (req, res) => {
  try {
    const inputData = req.query;
      const earliestDiary = await TreeDiary.findOne({
        attributes: ['date_published'],
        where: {
          '$tree.user_id$': req.userId,
          // ...(req.query?.area_id ? { '$tree.area_id$': req.query?.area_id } : {}),
          [Op.or]: [
            { ...(inputData?.area_id && inputData?.area_id?.length > 0 ? { '$tree.area_id$': inputData?.area_id } : {}) },
            { ...(inputData?.other_area ? { '$tree.area_id$': null } : {}) },
          ]
        },
        include: [
          {
            model: Tree,
            as: 'tree', 
            attributes: []
          }
        ],
        order: [['date_published', 'ASC']],
        // order: [['date_published', 'DESC']]
      });     
      res.json( earliestDiary );
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};

function countStatus(data) {
  let result = statusName.map((ele, index) => {
    return {
      name_status: ele,
      status_code: index,
      bgColor: themeMonth[index],
      iconColor: iconColor[index],
      count: 0
    }
  })
  data.forEach((ele, index) => {
    let treeDiaryArr = ele?.dataValues?.diaries
    if (treeDiaryArr?.length === 0) {
      // result[0]['count'] += 1
    }
    else {
      let status_code = treeDiaryArr[0]?.dataValues?.status;
      result[status_code]['count'] += 1
    }
  });
  return result;
}

self.getNumberStatuses = async (req, res) => {
  try {
    let data = await Tree.findAll({
      attributes: ['id'],
      where: {
        user_id: req.userId
      },
      include: [
        {
          model: db.TreeDiary, as: 'diaries',
          attributes: ['status'],
          where: {
            date_published: {
              [Sequelize.Op.lte]: new Date()
            }
          },
          order: [['date_published', 'DESC']],
          limit: 1
        },
      ]
    })
    res.json( countStatus(data) );
  } catch (error) {
    console.error(error);
    res.status(STATUS_CODE.INTERNAL_ERROR).json("Server Error");
  }
};
    ////////////////////////////////////////////////////////

    return self;

}
