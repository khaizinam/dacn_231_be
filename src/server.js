const dotenv = require("dotenv");
dotenv.config();
try {
  process.on("uncaughtException", (err) => {
    console.log("UNCAUGHT EXCEPTION! 💥");
    console.log("err mesage : ", err);
  });
} catch (error) {
  console.log("err mesage 2: ", error);
}

// const { CronJob } = require('cron') ;

// const job = new CronJob(
// 	'*/12 * * * *', // cronTime
// 	function () {
// 		console.log('You will see this message every 12 min');
// 	}, // onTick
// 	null, // onComplete
// 	true, // start
// 	'America/Los_Angeles' // timeZone
// );

const app = require("./app");

const port = process.env.PORT;
const server = app.listen(port, function () {
  console.log(`Listening : http://localhost:${port}/`);
});
