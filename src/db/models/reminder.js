"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Reminder extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Reminder.belongsToMany(models.Tree, {
        through: "reminder_tree",
        as: "trees",
        foreignKey: "reminder_id",
      });
      models.Tree.belongsToMany(Reminder, {
        through: "reminder_tree",
        as: "reminders",
        foreignKey: "tree_id",
      });
    }
  }
  Reminder.init(
    {
      time: DataTypes.TIME,
      loop: DataTypes.STRING,
      date_start: DataTypes.DATE,
      date_end: DataTypes.DATE,
      user_id: DataTypes.BIGINT,
      note: DataTypes.TEXT,
    },
    {
      sequelize,
      tableName: "reminders",
      modelName: "Reminder",
    }
  );
  return Reminder;
};
