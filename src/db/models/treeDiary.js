"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class TreeDiary extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      TreeDiary.belongsTo(models.Tree, {
        foreignKey: "tree_id",
        targetKey: "id",
        as: "tree",
      });
    }
  }
  TreeDiary.init(
    {
      tree_id: DataTypes.BIGINT,
      plant_id: DataTypes.BIGINT,
      stage_no: DataTypes.INTEGER,
      note: DataTypes.TEXT,
      weather: DataTypes.STRING,
      status: DataTypes.INTEGER,
      date_published: DataTypes.STRING,
      image_url: DataTypes.STRING,
    },
    {
      sequelize,
      tableName: "tree_diaries",
      modelName: "TreeDiary",
    }
  );
  return TreeDiary;
};
