"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Areas extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Areas.belongsTo(models.Plants, {
        foreignKey: "user_id",
        targetKey: "id",
        as: "users",
      });
    }
  }
  Areas.init(
    {
      no: DataTypes.INTEGER,
      name: DataTypes.TEXT,
      user_id: DataTypes.BIGINT,
      pump_code: DataTypes.TEXT,
      soil_code: DataTypes.TEXT,
      created_at: DataTypes.STRING,
    },
    {
      sequelize,
      tableName: "areas",
      modelName: "Area",
    }
  );
  return Areas;
};
