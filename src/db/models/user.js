"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  User.init(
    {
      email: DataTypes.STRING,
      full_name: DataTypes.STRING,
      password: DataTypes.STRING,
      full_name: DataTypes.STRING,
      phone_number: DataTypes.STRING,
      mail_contact: DataTypes.STRING,
      location: DataTypes.STRING,
      role: DataTypes.STRING,
      create_at: DataTypes.STRING,
    },
    {
      sequelize,
      tableName: "users",
      modelName: "User",
    }
  );
  return User;
};
