"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class ReminderTree extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  ReminderTree.init(
    {
      reminder_id: DataTypes.BIGINT,
      tree_id: DataTypes.BIGINT,
    },
    {
      sequelize,
      tableName: "reminder_tree",
      modelName: "ReminderTree",
    }
  );
  return ReminderTree;
};
