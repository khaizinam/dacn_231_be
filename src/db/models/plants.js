"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Plants extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Plants.hasMany(models.PlantPosts, {
        foreignKey: "plant_id",
        as: "introduction",
        onDelete: "CASCADE",
      });
      Plants.hasMany(models.PlantStage, {
        foreignKey: "plant_id",
        as: "stages",
        onDelete: "CASCADE",
      });

      Plants.hasMany(models.Tree, { foreignKey: "plant_id" });
    }
  }
  Plants.init(
    {
      name: DataTypes.STRING,
      avatar: DataTypes.STRING,
      description: DataTypes.STRING,
      category: DataTypes.STRING,
      updated_at : DataTypes.STRING,
    },
    {
      sequelize,
      tableName: "plants",
      modelName: "Plants",
    }
  );
  return Plants;
};
