"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class PlantPosts extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      PlantPosts.belongsTo(models.Plants, { foreignKey: 'plant_id', targetKey: 'id', as: 'plant' });
    }
  }
  PlantPosts.init(
    {
      no: DataTypes.INTEGER,
      plant_id: DataTypes.BIGINT,
      title: DataTypes.TEXT,
      content: DataTypes.TEXT,
      modified_at: DataTypes.STRING,
      created_at: DataTypes.STRING,
    },
    {
      sequelize,
      tableName: "plant_posts",
      modelName: "PlantPosts",
    }
  );
  return PlantPosts;
};
