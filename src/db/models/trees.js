"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Trees extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Trees.belongsTo(models.Plants, {
        foreignKey: "plant_id",
        targetKey: "id",
        as: "plant",
      });

      Trees.belongsTo(models.Area, {
        foreignKey: "area_id",
        targetKey: "id",
        as: "area",
      });
      Trees.hasMany(models.TreeDiary, {
        foreignKey: "tree_id",
        as: "diaries",
        onDelete: "CASCADE",
      });
    }
  }
  Trees.init(
    {
      name: DataTypes.TEXT,
      user_id: DataTypes.BIGINT,
      plant_id: DataTypes.BIGINT,
      area_id: DataTypes.INTEGER,
      description: DataTypes.TEXT,
      modified_at: DataTypes.STRING,
      created_at: DataTypes.STRING
    },
    {
      sequelize,
      tableName: "trees",
      modelName: "Tree",
    }
  );
  return Trees;
};
