"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class PlantStage extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      PlantStage.belongsTo(models.Plants, { foreignKey: 'plant_id', targetKey: 'id', as: 'plant' });
    }
  }
  PlantStage.init(
    {
      no: DataTypes.INTEGER,
      name: DataTypes.TEXT,
      description: DataTypes.TEXT,
      humidity_above: DataTypes.INTEGER,
      humidity_below: DataTypes.INTEGER,
      range: DataTypes.INTEGER,
      plant_id: DataTypes.BIGINT,
      grow_types: DataTypes.TEXT,
      img_url: DataTypes.TEXT,
    },
    {
      sequelize,
      tableName: "plant_stages",
      modelName: "PlantStage",
    }
  );
  return PlantStage;
};
