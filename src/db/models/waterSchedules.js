"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class WaterSchedule extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  WaterSchedule.init(
    {
      mode: DataTypes.STRING,
      time_start: DataTypes.STRING,
      time_end: DataTypes.STRING,
      soil_up: DataTypes.INTEGER,
      soil_down: DataTypes.INTEGER,
      pump_power: DataTypes.BOOLEAN,
      area_id: DataTypes.BIGINT,
    },
    {
      sequelize,
      tableName: "water_schedules",
      modelName: "WaterSchedule",
    }
  );
  return WaterSchedule;
};
