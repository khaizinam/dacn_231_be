"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class PlantFeedBack extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {}
  }
  PlantFeedBack.init(
    {
      plant_id: DataTypes.BIGINT,
      author_id: DataTypes.BIGINT,
      content: DataTypes.TEXT,
      is_seen: DataTypes.BOOLEAN,
      time_seen: DataTypes.DATE,
      created_at: DataTypes.DATE,
    },
    {
      sequelize,
      tableName: "plant_feed_back",
      modelName: "PlantFeedBack",
    }
  );
  return PlantFeedBack;
};
