"use strict";
/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("Users", {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      email: { type: Sequelize.STRING },
      fullName: { type: Sequelize.STRING },
      password: { type: Sequelize.STRING },
      phoneNumber: { type: Sequelize.STRING },
      roles: { type: Sequelize.STRING },
      curentLog: { type: Sequelize.BIGINT },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("Users");
  },
};
