const express = require("express");
const rateLimit = require("express-rate-limit");
const cors = require("cors");
const cookieParser = require("cookie-parser");
const morgan = require("morgan");
const helmet = require("helmet");

// Limit requests from same API
// 30 req /1 min

const app = express();
app.use(cors());
app.use(helmet());
app.use(
  helmet.contentSecurityPolicy({
    directives: {
      defaultSrc: ["'self'", "https:", "http:", "data:", "ws:"],
      baseUri: ["'self'"],
      fontSrc: ["'self'", "https:", "http:", "data:"],
      scriptSrc: [
        "'self'",
        "https:",
        "http:",
        "blob:",
        "https://js.stripe.com",
      ],
      styleSrc: ["'self'", "'unsafe-inline'", "https:", "http:"],
    },
  })
);
// Limit requests from same API
// 30 req /1 min
const limiter = rateLimit({
  max: 40,
  windowMs: 60 * 1000,
  message: "Too many requests from this IP, please try again in 1 min!",
});
app.use("/api", limiter);
app.use(express.json({ limit: "10MB" }));
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(morgan("short"));

// ROUTES
const userRouter = require("./routes/userRoutes");
const uploadRouter = require("./routes/uploadRoutes");
const plantRoutes = require("./routes/plantRoutes");
const gardenRoutes = require("./routes/gardenRoutes");
const reminderRoutes = require("./routes/reminderRoutes");
const chartRoutes = require("./routes/ChartRoute");
const statisticsRoutes = require("./routes/statisticsRoutes");

app.use("/api/auth", userRouter);
app.use("/api/garden", gardenRoutes);
app.use("/api/plant", plantRoutes);
app.use("/api/uploads", uploadRouter);
app.use("/api/reminder", reminderRoutes);
app.use("/api/chart", chartRoutes);
app.use("/api/statistics", statisticsRoutes);

app.all("*", (req, res, next) => {
  res.status(400).send("Not found that url!");
});

module.exports = app;
