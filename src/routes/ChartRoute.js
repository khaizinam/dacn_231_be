const express = require("express");
const {ChartController} = require("../controller/ChartController");
const chartController = ChartController();
const router = express.Router();
const authController = require("../controller/authController");

/* ----- PROTECTED GATEWAY ----- */
router.use(authController.protect);

router.get("/tree-status", chartController.getStatusOfTreeData);
router.get("/firt-diary-time", chartController.getFirstDiary);
router.get("/number-tree-status", chartController.getNumberStatuses);
// router.post("/", chartController.getFilter);

/* ----------------------------- */
module.exports = router;
