const express = require("express");
const userController = require("../controller/userController");
const authController = require("../controller/authController");
const router = express.Router();

/* ------- OPEN GATEWAY ------- */
router.post("/login", authController.login);
router.post("/sign-up", authController.signup);
router.get("/permission", authController.initPermission);
/* ---------------------------- */

/* ----- PROTECTED GATEWAY ----- */
router.use(authController.protect);
//--------------------------------
router.get("/current", userController.getCurrent);
router.get("/list-user", userController.getUsers);
router.put("/user/password", userController.changePassword);
router.put("/user", userController.updateProfile);
/* ----------------------------- */
module.exports = router;
