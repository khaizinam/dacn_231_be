const express = require("express");
const uploadCloud = require("../cloudinary/configAdmin");
const router = express.Router();

router.post("/", uploadCloud.single("image"), async (req, res, next) => {
  try {
    res.json(req.file.path);
  } catch (error) {
    console.log(error);
    res.status(500).json("Error");
  }
});

module.exports = router;
