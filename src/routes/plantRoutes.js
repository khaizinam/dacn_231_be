const express = require("express");
const plantController = require("../controller/plantController");
const uploadCloud = require("../cloudinary/config");
const router = express.Router();

/* ------- OPEN GATEWAY ------- */
router.get("/category-list", plantController.getCategoryList);
router.get("/:plantId", plantController.getPlant);

// -------------- STAGE --------------
router.get("/stage/:plantId", plantController.getStage);
router.post(
  "/stage/:plantId",
  uploadCloud.single("img_url"),
  plantController.createStage
);
router.put("/stage/arrange/:plantId", plantController.changeOrder);
router.put(
  "/stage/:stageId",
  uploadCloud.single("new_img"),
  plantController.updateStage
);
router.delete("/stage/:stageId", plantController.deleteStage);

// -------------- POST ---------------
router.post("/post/:plantId", plantController.createPost);
router.put("/post/:postId", plantController.updatePost);
router.delete("/post/:postId", plantController.deletePost);
router.put("/description/:plantId", plantController.updateDescription);

router.delete("/:plantId", plantController.deletePlant);
router.put(
  "/:plantId",
  uploadCloud.single("image"),
  plantController.updatePlant
);
router.post("/", uploadCloud.single("image"), plantController.createPlant);
router.get("/", plantController.getPlantsList);

/* --------  FEED BACK  ----------- */

router.post("/feed-back", plantController.storeFeedBack);
router.post("/feed-backs", plantController.getFeedBackByFilter);
router.put("/feed-back/:id", plantController.seenFeedBack);
router.put("/feed-back", plantController.updateFeedBack);
router.delete("/feed-back", plantController.storeFeedBack);
module.exports = router;
