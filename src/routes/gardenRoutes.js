const express = require("express");
const authController = require("../controller/authController");
const gardenController = require("../controller/gardenController");
const uploadCloud = require("../cloudinary/config");
const { DiaryController } = require("../controller/DiaryController");
const diaryController = DiaryController();
const router = express.Router();

router.use(authController.protect);
router.get("/", gardenController.getGarden);
router.get("/stages/:tree_id", gardenController.getTreeStages);
router.delete("/areas/:id", gardenController.deleteArea);
router.get("/areas", gardenController.getAreas);
router.put("/areas/:id", gardenController.updateArea);
router.post("/areas", gardenController.createArea);

router.put("/tree/:id", gardenController.updateTree);
router.delete("/tree/:id", gardenController.deleteTree);
router.post("/tree/", gardenController.addTree);

// DIARY
router.get("/diary/:tree_id", diaryController.get);
router.post(
  "/diary/:tree_id",
  uploadCloud.single("image"),
  diaryController.store
);
router.put(
  "/diary/:diary_id",
  uploadCloud.single("image"),
  diaryController.update
);
router.delete("/diary/:diary_id", diaryController.destroy);
/* ----------------------------- */
module.exports = router;
