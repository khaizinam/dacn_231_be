const express = require("express");
const reminderController = require("../controller/reminderController");
const authController = require("../controller/authController");
const router = express.Router();

router.use(authController.protect);
router.get("/", reminderController.getReminder);
router.post("/", reminderController.createReminder);
router.delete("/:id", reminderController.deleteReminder);
router.put("/:id", reminderController.updateReminder);

module.exports = router;
