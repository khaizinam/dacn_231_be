const express = require("express");
const statisticsController = require("../controller/statisticsController");
const authController = require("../controller/authController");
const uploadCloud = require("../cloudinary/config");
const router = express.Router();

/* ----- PROTECTED GATEWAY ----- */
router.use(authController.protect);

/* ------- OPEN GATEWAY ------- */
router.get("/tree-status", statisticsController.getStatusOfTreeData);
router.get("/firt-diary-time", statisticsController.getFirstDiary);
router.get("/number-tree-status", statisticsController.getNumberTreeStatuses);
router.get("/get-new-subscriber-statistical-data", statisticsController.getNewSubscriberStatisticalData);
router.get("/get-first-user-register-time", statisticsController.getFirstUserRegisterTime);
router.get("/get-general-statistic-data", statisticsController.getGeneralStatisticData);
router.get("/get-tree-statistical-data", statisticsController.getTreeStatisticalData);
router.get("/get-first-tree-create-time", statisticsController.getFirstTreeCreateTime);
router.get("/admin-plants/:plant_id", statisticsController.getNumberPlants);

module.exports = router;
