con
const db = require("../db/models/index");
const { Op, Sequelize } = require("sequelize");

module.exports.DiaryModel = function () {
    const self = {
        model: db['plant_feed_back'],
    }
    self.get_fbk_is_seen = async function (is_seen=false){
        try {
            const r = await self.model.count({where:{is_seen}});
            return r;
        } catch (error) {return 0;}
    }
    return self;
}