
module.exports.DataTable = function (headerRow = []) {
    const self = {
        numCols: headerRow.length + 1,
        numRows: 1,
        data: [],
    }
    self.data.push(['labels', ...headerRow]);

    self.add = function (name, row = []) {
        if (row.length === 0) return false;
        if (row.length !== self.numCols - 1) return false;
        self.data.push([name, ...row]);
        self.numRows++;
        return true;
    }
    self.get = function () {
        return {
            data: self.data,
            numCols: self.numCols,
            numRows: self.numRows,
        };
    }
    return self;
}