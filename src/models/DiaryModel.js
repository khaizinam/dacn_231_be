
const db = require("../db/models/index");
const { Op, Sequelize } = require("sequelize");

module.exports.DiaryModel = function () {
    
    const self = {
        model: db['TreeDiary'],
    }
    self.store = async function (data){
        const result = await self.model.upsert(data);
        return result;
    }

    self.filter = async function (query){
        const result = self.model.findOne(query);
        return result;
    }

    self.find_by_id = async function (id ){
        try {
            const result = await self.model.findOne({ where: { id } });
            return result;
        } catch (error) {
            return null;
        }
    }
    self.update_by_id = async function (data, id){
        try {
            const result = await self.model.update(data, {where: {id}});
            return result;
        } catch (error) {
            return [];
        }
    }

    self.destroy_by_id = async function (id){
        try {
            const result = await self.model.destroy({where : {id}})
            return true;
        } catch (error) {
            return false;
        }
    }

    async function get_distinct_tree(plant_id){
        try {
            const r = await self.model.findAll({ where: {plant_id}, order: [["tree_id"]]});
            const a = []; r.forEach((item) => {a.push(item.dataValues.tree_id);}); 
            return new Set(a);
        } catch (e) {return new Set([]);}
    }

    self.get_current_status = async function (tree_id=0){
        try {
            const ld = await self.model.findOne({where: {tree_id}, order: [["date_published", "DESC"]],});
            return { cs: ld ? ld.dataValues.status : -1, uid : ld ? ld.dataValues.user_id : null};
        } catch (e) {return null;}
    }

    self.get_num_by_plant_id = async function ( plant_id=0){
        if(plant_id===0) return 0; 
        try { 
            const s = [0,0,0,0]; 
            const ta = await get_distinct_tree(plant_id); 
            if(ta.size === 0) return [0, 0,0,0,0]; 
            else{
                const u_ls = [];
                for(const e of ta){ 
                    const {cs,uid} = await self.get_current_status(e); //current status (cs) , user_id (uid)
                    if (uid!==null) u_ls.push(uid); // push to list user if not null
                    if (cs>=0 && cs<=3)s[cs]++; // count to status with current status (cs) 
                };
                const n_uls = new Set(u_ls);
                return [n_uls.size ,...s];
            };
        } catch (e) {return [0,0,0,0,0];}
    }

    return self;
}