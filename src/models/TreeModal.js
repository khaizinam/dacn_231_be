
const db = require("../db/models/index");
const { Op } = require("sequelize");
const { DiaryModel } = require("./DiaryModel");

module.exports.TreeModal = function () {
    
    const self = {
        model: db['Tree'],
        diaryModel : db['TreeDiary']
    }
    
    self.get_current_status = async function (tree_id){
        try {
            const diaryModal = DiaryModel();
            const { cs, uid } = await diaryModal.get_current_status(tree_id);
            return cs;
        } catch (error) {return 0;}
    }

    self.get_by_user_area_date = async function ({ user_id = 0, areas = [], date_search = "" }) {
        const data = await self.model.findAll({
            attributes: ['id'],
            where: { user_id: user_id },
            include: [
                {
                    model: self.diaryModel,
                    as: 'diaries',
                    where: {
                        date_published: {
                            [Op.lte]: date_search
                        }
                    },
                    order: [['date_published', 'DESC']],
                    limit: 1,
                    nest: true
                },
            ],
            nest: true
        });
        return data;
    }

    self.get_by_date_publish = async function (date_publish,user_id) {
        const data = await self.model.findAll({
            attributes: ['id'],
            where: { user_id },
            include: [
                {
                    model: self.diaryModel,
                    as: 'diaries',
                    where: {
                        date_published: {
                            [Op.lte]: date_publish
                        }
                    },
                    order: [['date_published', 'DESC']],
                    limit: 1,
                    nest: true
                },
            ],
            nest: true
        });
        return data;
    }
    return self;
}